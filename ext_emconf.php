<?php
$EM_CONF[$_EXTKEY] = array(
    'title' => 'Schwaz Template',
    'description' => 'Schwaz Template',
    'category' => 'fe',
    'author' => 'David Außerlechner',
    'author_email' => 'david.ausserlechner@gmail.com',
    'state' => 'stable',
    'version' => '1.0.0',
    'constraints' =>
        array(
            'depends' =>
                array(
                    'typo3' => '10.4.0-10.4.99',
                ),
            'conflicts' =>
                array(),
            'suggests' =>
                array(),
        ),
);

