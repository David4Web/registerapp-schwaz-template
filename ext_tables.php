<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

call_user_func(function(){

    //$_VENDOR_EXT = 'Ausserlechner.SchwazTemplate';
    $_EXTENSION = 'schwaz_template';

    //$_ExtensionUtility = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::class;
    $_ExtensionManagementUtility = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::class;

    /********************************/

    $_ExtensionManagementUtility::addStaticFile($_EXTENSION, 'Configuration/TypoScript', 'Schwaz Template');
});

