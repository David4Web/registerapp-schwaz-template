<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(function(){

    /********************************/

    $_ExtensionManagementUtility    = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::class;
   //$_ExtensionUtility              = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::class;
    $_GeneralUtility                = \TYPO3\CMS\Core\Utility\GeneralUtility::class;
    $_PageRenderer                  = \TYPO3\CMS\Core\Page\PageRenderer::class;

    //$_VENDOR_EXT = 'Ausserlechner.SchwazTemplate';
    $_EXTENSION = 'schwaz_template';

    /********************************/

    $_ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:'.$_EXTENSION.'/Configuration/TSconfig/Page.tsconfig">');

    if(TYPO3_MODE ==="FE"){
        // VUE BUNDLE
        $PageRenderer = $_GeneralUtility::makeInstance($_PageRenderer);
        $PageRenderer->addJsFooterLibrary("FE_VUE_BUNDLE", "/typo3conf/ext/".$_EXTENSION."/Resources/Public/webapp/webapp.bundle.js");
        $PageRenderer->addCssFile("/typo3conf/ext/".$_EXTENSION."/Resources/Public/webapp/webapp.bundle.css");
    }
});
